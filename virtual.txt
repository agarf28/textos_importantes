venv no ubuntu
faça:
python -m venv <nome_do_seu_ambiente>-env
python -m venv my-projeto-env

faça:
source my-project-env/bin/activate

para desativar venv
faça:
deactivate

faça:
pip list

atualize o pip:
faça:
pip install --upgrade pip

pip install #escolhe a biblioteca.

pip list

Use requirements.txt
faça:
nano requirements.txt
coloque os pacotes que vai usar e a versao
se fo a ultima nao precisa colocar nada.
salva.

faça:
pip install -r requirements.txt

para ver todas as bibliotecas
instaladas no ambiente 
faça:
pip freeze

para desinstalar um programa
faça:
pip uninstall programa
desse modo as dependencia não são desinstaladas.

instale a ferramenta pip autoremove
ela remove o programa e as dependencias
faça:
pip install pip-autoremove

e para desinstalar um programa
faça:
pip-autoremove programa

# veja a ferramenta que mostra as dependencias de cada pacote
faça:
pip install pipdeptree

e depois
faça:

pipdeptree

o que não vai pra produçao fica num arquivo separado
requirements_dev.txt
faça:
nano requirements_dev.txt
como fica depois que você entra no requirements_dev.txt
faça:
# instala as bibliotecas de produção
-r requirements.txt

# Essas são as de desenvolvimento
black
ipdb
ipython
pytest

depois faça o comando abaixo para instalar todas as coisas
de produção e de desenvolvimento.
faça:
pip install -r requirements_dev.txt

parei em uma hora e 10 minutos.




